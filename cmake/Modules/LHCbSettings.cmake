###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Extend Geant4 version with the version of LHCb patch
set(${PROJECT_NAME}_VERSION_TWEAK  1)
set(${PROJECT_NAME}_VERSION "${${PROJECT_NAME}_VERSION}.${${PROJECT_NAME}_VERSION_TWEAK}")


include(FindDataPackage)
find_data_package(Geant4Files ${Geant4_VERSION_MAJOR}${Geant4_VERSION_MINOR}.0)


# FIXME: this should match the version used by ROOT (LCG version + compiler version)
if( (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS "13.0")
    OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS "16.0") )
    set(LHCB_DEFAULT_CXXSTD 17)
else()
    set(LHCB_DEFAULT_CXXSTD 20)
endif()
set(GEANT4_BUILD_CXXSTD ${LHCB_DEFAULT_CXXSTD} CACHE STRING "C++ Standard")

set(OpenGL_GL_PREFERENCE "GLVND" CACHE STRING
    "Preferred GL library to use (see https://cmake.org/cmake/help/v3.12/policy/CMP0072.html)")

# This is actually only used if GEANT4_BUILD_MULTITHREADED is true
set(GEANT4_BUILD_TLS_MODEL global-dynamic CACHE STRING "")

set(GEANT4_INSTALL_DATA OFF CACHE BOOL "")
if(Geant4Files_FOUND)
    set(GEANT4_INSTALL_DATADIR ${Geant4Files_ROOT_DIR}/data CACHE PATH "")
endif()
set(GEANT4_USE_SYSTEM_CLHEP ON CACHE BOOL "")
set(GEANT4_USE_XM ON CACHE BOOL "")
set(GEANT4_USE_GDML ON CACHE BOOL "")
set(GEANT4_USE_OPENGL ON CACHE BOOL "")
set(GEANT4_USE_OPENGL_X11 ON CACHE BOOL "")
set(GEANT4_USE_RAYTRACER_X11 ON CACHE BOOL "")
set(GEANT4_USE_NETWORKVRML ON CACHE BOOL "")
set(GEANT4_USE_NETWORKDAWN ON CACHE BOOL "")
set(GEANT4_USE_INVENTOR OFF CACHE BOOL "")
set(GEANT4_BUILD_VERBOSE_CODE ${G4VERBOSE} CACHE BOOL "")
set(GEANT4_INSTALL_EXAMPLES OFF CACHE BOOL "")

# Optionally enable compatibility with old-style LHCb CMake configurations, via helper module
option(GAUDI_LEGACY_CMAKE_SUPPORT "Enable compatibility with old-style CMake builds" "$ENV{GAUDI_LEGACY_CMAKE_SUPPORT}")

if(GAUDI_LEGACY_CMAKE_SUPPORT)
    # make sure we have a BINARY_TAG CMake variable set
    # (duplication from LegacyGaudiCMakeSupport.cmake, but needed here otherwise it's too late)
    if(NOT BINARY_TAG)
        if(NOT "$ENV{BINARY_TAG}" STREQUAL "")
            set(BINARY_TAG $ENV{BINARY_TAG})
        elseif(LHCB_PLATFORM)
            set(BINARY_TAG ${LHCB_PLATFORM})
        else()
            message(AUTHOR_WARNING "BINARY_TAG not set")
        endif()
    endif()

    # default install prefix when building in legacy mode
    if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/InstallArea/${BINARY_TAG}"
        CACHE PATH "Install prefix" FORCE)
    endif()

    find_file(legacy_cmake_config_support NAMES LegacyGaudiCMakeSupport.cmake)
    if(NOT legacy_cmake_config_support)
        message(FATAL_ERROR "GAUDI_LEGACY_CMAKE_SUPPORT set to TRUE, but cannot find LegacyGaudiCMakeSupport.cmake")
    endif()

    set_property(GLOBAL APPEND PROPERTY ${PROJECT_NAME}_ENVIRONMENT
        PREPEND PATH ${CMAKE_INSTALL_PREFIX}/bin
        PREPEND LD_LIBRARY_PATH ${CMAKE_INSTALL_PREFIX}/lib
        PREPEND ROOT_INCLUDE_PATH ${CMAKE_INSTALL_PREFIX}/include/Geant4
    )
endif()

# Note: this enables building of tests, but not the Geant4 ones (that require GEANT4_ENABLE_TESTING and GEANT4_BUILD_TESTS)
include(CTest)

add_compile_options(
    # Suppress very extensive warnings enabled with our compilation options (but not by Geant4).
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-suggest-override>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-non-virtual-dtor>
)
