#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import subprocess

"""
     Stirring application for the RICH test beam simulation test (aka RichTbSimH).
"""


def main():
    print('Starting RICH test beam simulation test...')

    workspace = os.path.join(os.sep, os.getcwd(), 'G4RichTbSimHTestOutput')
    if not os.path.exists(workspace):
        os.makedirs(workspace)

    os.chdir(workspace)
    print('Working directory:', os.getcwd())

    print('Executing the test...')
    cmd = 'G4RichTbSimHTest'
    subprocess.call(cmd, shell=True)


if __name__ == '__main__':
    main()

# EOF
