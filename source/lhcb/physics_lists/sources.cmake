geant4_add_module(G4_LHCb_phys_lists
    PUBLIC_HEADERS
        LHCbG4PhysLists/G4EmStandardPhysics_LHCbTest.hh
        LHCbG4PhysLists/G4EmStandardPhysics_option1LHCb.hh
        LHCbG4PhysLists/G4EmStandardPhysics_option1NoApplyCuts.hh
    SOURCES
        G4EmStandardPhysics_LHCbTest.cc
        G4EmStandardPhysics_option1LHCb.cc
        G4EmStandardPhysics_option1NoApplyCuts.cc
)

geant4_module_link_libraries(G4_LHCb_phys_lists
    PUBLIC
        G4global
        G4run
    PRIVATE
        G4physicslists
)
