add_executable(G4GammaToDiLeptonConversionTest
    G4GammaToDiLeptonConversionTest.cc
    src/DetectorConstruction.cc
    src/DetectorMessenger.cc
    src/PhysicsList.cc
    src/PhysicsListMessenger.cc
    src/PrimaryGeneratorAction.cc
    src/PrimaryGeneratorMessenger.cc
    src/RunAction.cc
    src/StackingAction.cc
    src/StepMax.cc
    src/StepMaxMessenger.cc
    src/SteppingAction.cc
    src/SteppingVerbose.cc
)
target_include_directories(G4GammaToDiLeptonConversionTest
    PRIVATE include
)
target_link_libraries(G4GammaToDiLeptonConversionTest
    PRIVATE
        G4global
        G4physicslists
        G4run
)
install(TARGETS G4GammaToDiLeptonConversionTest)
install(
    PROGRAMS
        scripts/G4GammaToDiLeptonConversionTest.py
        scripts/run_gammaconv_test.sh
    TYPE BIN
)
