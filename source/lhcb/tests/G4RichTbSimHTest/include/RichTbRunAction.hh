#ifndef RichTbRunAction_h
#define RichTbRunAction_h 1

#include "globals.hh"
#include "G4UserRunAction.hh"
#include "RichTbAnalysisManager.hh"

class G4Timer;
class G4Run;

class RichTbRunAction : public G4UserRunAction {
public:
  RichTbRunAction();
  virtual ~RichTbRunAction();

public:
  void BeginOfRunAction(const G4Run *aRun) override;
  void EndOfRunAction(const G4Run *aRun) override;
  RichTbAnalysisManager *getAnalysisMRun() { return ranalysisManager; }

private:
  G4Timer *timer;
  RichTbAnalysisManager *ranalysisManager;
};
#endif /*RichTbRunAction_h */
