#ifndef RichTbGraphics_h
#define RichTbGraphics_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "RichTbUpgradeDetectorConstruction.hh"

class RichTbGraphics {

public:
  RichTbGraphics();
  RichTbGraphics(RichTbDetectorConstruction *);
  virtual ~RichTbGraphics();

  void setAllGraphicsAttributes();
  void setRichTbHallGraphicsAttibutes();

private:
  RichTbDetectorConstruction *curDetector;
};
#endif /*RichTbGraphics_h */
