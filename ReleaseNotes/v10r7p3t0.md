2023-11-08 Geant4 v10r7p3t0
===

This is the build of Geant4 in the LHCb environment with LHCb patches and tests.

This version is based on  10.7.p03
([original release notes](ReleaseNotes4.10.7.html) and [patch notes](Patch4.10.7-3.txt))

This version uses
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on `master` branch.
Built relative to Geant4 [v10r6p2t7](/../../tags/v10r6p2t7), with the following changes:


### New features ~"new feature"

- Update to Geant4 10.7.3, !82 (@clemenci)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- MT build hadronic test fix, !100 (@dpopov)
- MT build msc test fix, !99 (@dpopov)
- MT build calo test fix, !98 (@dpopov)
- Fix non-virtual-dtor warnings from G4VFastSimSensitiveDetector, !94 (@mimazure)


### Other

- Fix compilation with gcc12 by backporting change from Geant4 v11, !92 (@kreps)
