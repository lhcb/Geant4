#ifndef INCLUDE_RICHTBUPGRADEEC_HH
#define INCLUDE_RICHTBUPGRADEEC_HH 1

// Include files
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "RichTbUpgradePhDetSupFrame.hh"

/** @class RichTbUpgradeEC RichTbUpgradeEC.hh include/RichTbUpgradeEC.hh
 *
 *
 *  @author Sajan Easo
 *  @date   2014-10-23
 */
class RichTbUpgradeEC {
public:
  /// Standard constructor
  RichTbUpgradeEC(RichTbUpgradePhDetSupFrame *rTbPhotSupFrame);

  virtual ~RichTbUpgradeEC(); ///< Destructor
  void constructRichTbUpgradeEC();
  void constructRichTbUpgradeECSupport();
  void constructRichTbUpgradeSingleEC();
  void constructRichTbUpgradeSingleECSupport();
  void constructRichTbUpgradeEC15();
  void constructRichTbUpgradeECSupport15();

  G4LogicalVolume *getRichTbECLeftLVol() { return RichTbECLeftLVol; }
  G4LogicalVolume *getRichTbECRightLVol() { return RichTbECRightLVol; }
  G4VPhysicalVolume *getRichTbECLeftPVol() { return RichTbECLeftPVol; }
  G4VPhysicalVolume *getRichTbECRightPVol() { return RichTbECRightPVol; }

  // Upgrade 2015
  G4LogicalVolume *getRichTbECBottomLeftLVol() { return RichTbECBottomLeftLVol; }
  G4LogicalVolume *getRichTbECBottomRightLVol() { return RichTbECBottomRightLVol; }
  G4VPhysicalVolume *getRichTbECBottomLeftPVol() { return RichTbECBottomLeftPVol; }
  G4VPhysicalVolume *getRichTbECBottomRightPVol() { return RichTbECBottomRightPVol; }

  RichTbUpgradePhDetSupFrame *getRTbPhotSupFrame() { return aRTbPhotSupFrame; }

  G4LogicalVolume *getRichTbECSupLVol() { return RichTbECSupLVol; }

protected:
private:
  G4LogicalVolume *RichTbECLeftLVol;
  G4LogicalVolume *RichTbECRightLVol;
  G4VPhysicalVolume *RichTbECLeftPVol;
  G4VPhysicalVolume *RichTbECRightPVol;

  // Upgrade 2015
  G4LogicalVolume *RichTbECBottomLeftLVol;
  G4LogicalVolume *RichTbECBottomRightLVol;
  G4VPhysicalVolume *RichTbECBottomLeftPVol;
  G4VPhysicalVolume *RichTbECBottomRightPVol;

  RichTbUpgradePhDetSupFrame *aRTbPhotSupFrame;
  G4LogicalVolume *RichTbECSupLVol;

  G4VPhysicalVolume *RichTbECSupLeftPVol;
  G4VPhysicalVolume *RichTbECSupRightPVol;
  G4VPhysicalVolume *RichTbECSupBottomLeftPVol;
  G4VPhysicalVolume *RichTbECSupBottomRightPVol;
};
#endif // INCLUDE_RICHTBUPGRADEEC_HH
