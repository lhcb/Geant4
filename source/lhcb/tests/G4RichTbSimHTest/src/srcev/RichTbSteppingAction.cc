#include "RichTbSteppingAction.hh"
#include "G4DynamicParticle.hh"
#include "G4Electron.hh"
#include "G4Material.hh"
#include "G4Navigator.hh"
#include "G4OpticalPhoton.hh"
#include "G4ParticleDefinition.hh"
#include "G4PionMinus.hh"
#include "G4Step.hh"
#include "G4SteppingManager.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4TransportationManager.hh"
#include "globals.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbBeamProperty.hh"
#include "RichTbCounter.hh"
#include "RichTbGeometryParameters.hh"
#include "RichTbMaterial.hh"
#include "RichTbMaterialParameters.hh"
#include "RichTbPhotonUserInfoAttach.hh"
#include "RichTbPrimaryGeneratorAction.hh"
#include "RichTbRunConfig.hh"

RichTbSteppingAction::RichTbSteppingAction() {
  G4cout << " Created user RichTbSteppingAction " << G4endl;

  //    ranalysisManager = RichTbAnalysisManager::getInstance();
  //   richtbRunConfig = RichTbRunConfig::getRunConfigInstance();
  //  rPrimGenAction = RPrimGenAction;
  //  PMTPhElectronKE = richtbRunConfig->getPMTPhElectronEnergy();
  //  uParticleChange = new G4VParticleChange();
}

RichTbSteppingAction::~RichTbSteppingAction() { ; }

void RichTbSteppingAction::UserSteppingAction(const G4Step *aStep) {

  RichTbRadiatorLensBoundaryIncidenceStep(aStep);

  RichTbPMTIncidenceStep(aStep);

  //    RichTbGenericHisto(aStep);
  // RichTbDebugHisto(aStep);

  RichTbCounter *aRCounter = RichTbCounter::getRichTbCounterInstance();
  //  RichTbAnalysisManager* aRAnalysisManager = RichTbAnalysisManager::getInstance();
  // RichTbRunConfig* aRunConfig =   RichTbRunConfig::getRunConfigInstance();

  G4StepPoint *pPreStepPoint = aStep->GetPreStepPoint();
  G4StepPoint *pPostStepPoint = aStep->GetPostStepPoint();
  const G4ThreeVector prePos = pPreStepPoint->GetPosition();
  const G4ThreeVector postPos = pPostStepPoint->GetPosition();
  // now test if the prestep is inside the vessel.

  if ((prePos.z() < RichTbVesselDnsZEnd && prePos.z() >= 0.0 * CLHEP::mm) &&
      (prePos.x() < RichTbVesselXPosExtreme && prePos.x() > RichTbVesselXNegExtreme) &&
      (prePos.y() < RichTbVesselYPosExtreme && prePos.y() > RichTbVesselYNegExtreme)) {

    // check to see if if we are a boundary.

    if (pPostStepPoint->GetStepStatus() == fGeomBoundary) {

      G4Track *aTrack = aStep->GetTrack();
      const G4DynamicParticle *aParticle = aTrack->GetDynamicParticle();
      const G4double aTrackEnergy = aParticle->GetKineticEnergy();

      // test for photons which are not already killed.

      if ((aParticle->GetDefinition() == G4OpticalPhoton::OpticalPhoton()) && (aTrackEnergy > 0.0)) {

        // G4double curPhotWavLenNano=PhotMomToWaveLength/(aTrackEnergy*CLHEP::nanometer);

        // check that valid phys vol exist to avoid particles exiting to universe.

        if (pPreStepPoint->GetPhysicalVolume() && pPostStepPoint->GetPhysicalVolume()) {

          G4String tpreVol = pPreStepPoint->GetPhysicalVolume()->GetName();
          G4String tpostVol = pPostStepPoint->GetPhysicalVolume()->GetName();

          //          G4String tpreVolFirstPart=
          //  (tpreVol.length() >=7 ) ? tpreVol(0,7) : tpreVol;
          //  G4String tpostVolFirstPart =
          //  (tpostVol.length() >= 7) ? tpostVol(0,7):tpostVol;

          //   G4cout <<"RichTbstep pre post vol "<<tpreVol
          // <<"  "<<tpostVol<<G4endl;

          // now for photons entering the mirror
          if (tpreVol == VesselPhysName && tpostVol == MirrorPhysName) {
            //  G4cout<<"StepAction: Entering mirror "<<G4endl;

            aRCounter->bumpnumPhEnterMirror();
            // if( aRAnalysisManager->getfhistoWEnterMirror() )
            //			aRAnalysisManager->getfhistoWEnterMirror()
            // ->fill(curPhotWavLenNano);
          }
          // now for photons exiting the mirror
          if (tpreVol == MirrorPhysName && tpostVol == VesselPhysName) {
            //  G4cout<<"StepAction: Entering mirror "<<G4endl;

            //   if( aRAnalysisManager->getfhistoWExitMirror() )
            //		aRAnalysisManager->getfhistoWExitMirror()
            // ->fill( curPhotWavLenNano);
          }
          // now for photons entering the ph det sup frame.
          if (tpreVol == VesselPhysName && tpostVol == GasQuWinPhysName) {

            // G4cout<<"StepAction: Entering  GasQuWin "<<G4endl;

            aRCounter->bumpnumPhEnterPhSupFrame();
            //     if( aRAnalysisManager->getfhistoWEnterPhSupFrame())
            //		aRAnalysisManager->getfhistoWEnterPhSupFrame()
            //  ->fill( curPhotWavLenNano);
          }
          if (tpreVol == GasQuWinPhysName && tpostVol == PhDetSupName) {
            // now exiting Gas qu Win

            aRCounter->bumpnumExitGasQuWin();

            //  if( aRAnalysisManager->getfhistoWExitGasQuWin())
            //	aRAnalysisManager->getfhistoWExitGasQuWin()
            //  ->fill( curPhotWavLenNano);
          }

          // now for photons enetering the PMT QW

          if (tpreVol == PMTSMasterPhysName && tpostVol == PMTQuartzPhysName) {
            //  G4cout<<"StepAction: Entering PMT QW "<<G4endl;

            aRCounter->bumpnumPhEnterAnPMTQW();

            G4TouchableHistory *CurTTB = (G4TouchableHistory *)(pPreStepPoint->GetTouchable());
            CurTTB->MoveUpHistory(1);
            G4int curPMTNum = CurTTB->GetVolume()->GetCopyNo();
            if (curPMTNum == 0) {
              //   G4cout<<"StepAction: Entering PMT0  QW "<<G4endl;

              // if( aRAnalysisManager->getfhistoWEnterPMT0QW() )
              //   aRAnalysisManager->getfhistoWEnterPMT0QW()
              //     ->fill(curPhotWavLenNano);
              aRCounter->bumpnumPhEnterPMT0QW();

            } else if (curPMTNum == 1) {
              //  G4cout<<"StepAction: Entering PMT1  QW "<<G4endl;

              //  if( aRAnalysisManager->getfhistoWEnterPMT1QW() )
              //   aRAnalysisManager->getfhistoWEnterPMT1QW()
              //    ->fill(curPhotWavLenNano);
              aRCounter->bumpnumPhEnterPMT1QW();
            } else if (curPMTNum == 2) {
              //  G4cout<<"StepAction: Entering PMT2  QW "<<G4endl;

              //		  if( aRAnalysisManager->getfhistoWEnterPMT2QW() )
              //   {
              //  G4cout<<"StepAction: Entering PMT2  QW fil histo  "
              // << G4endl;

              //     aRAnalysisManager->getfhistoWEnterPMT2QW()
              //		->fill(curPhotWavLenNano);
              //   }

              // G4cout<<"StepAction: Entering PMT2  QW bump counter "
              // <<G4endl;

              aRCounter->bumpnumPhEnterPMT2QW();
              //  G4cout<<"StepAction: Entering PMT2  QW counter bumped up"
              // <<G4endl;
            } else if (curPMTNum == 3) {
              //  G4cout<<"StepAction: Entering PMT3  QW "<<G4endl;

              //	  if( aRAnalysisManager->getfhistoWEnterPMT3QW() )
              //  {
              //  G4cout<<"StepAction: Entering PMT3  QW fil histo  "
              // <<G4endl;

              //      aRAnalysisManager->getfhistoWEnterPMT3QW()
              //		->fill(curPhotWavLenNano);
              //  }

              // G4cout<<"StepAction: Entering PMT2  QW bump counter "
              // <<G4endl;

              aRCounter->bumpnumPhEnterPMT3QW();
              //  G4cout<<"StepAction: Entering PMT2  QW counter bumped up"
              // <<G4endl;

            } else if (curPMTNum == 4) {
              //  G4cout<<"StepAction: Entering PMT4  QW "<<G4endl;

              //	  if( aRAnalysisManager->getfhistoWEnterPMT4QW() )
              //  {
              //  G4cout<<"StepAction: Entering PMT4  QW fil histo  "
              // <<G4endl;

              //     aRAnalysisManager->getfhistoWEnterPMT4QW()
              //		->fill(curPhotWavLenNano);
              //   }

              // G4cout<<"StepAction: Entering PMT4  QW bump counter "
              // <<G4endl;

              aRCounter->bumpnumPhEnterPMT4QW();
              //  G4cout<<"StepAction: Entering PMT4  QW counter bumped up"
              // <<G4endl;
            } else if (curPMTNum == 5) {
              //  G4cout<<"StepAction: Entering PMT5  QW "<<G4endl;

              //	  if( aRAnalysisManager->getfhistoWEnterPMT5QW() )
              //  {
              //  G4cout<<"StepAction: Entering PMT5  QW fil histo  "
              // <<G4endl;

              //    aRAnalysisManager->getfhistoWEnterPMT5QW()
              //		->fill(curPhotWavLenNano);
              //   }

              // G4cout<<"StepAction: Entering PMT5  QW bump counter "
              // <<G4endl;

              aRCounter->bumpnumPhEnterPMT5QW();
              //  G4cout<<"StepAction: Entering PMT5  QW counter bumped up"
              // <<G4endl;
            } else if (curPMTNum == 6)
              aRCounter->bumpnumPhEnterPMT6QW();
            else if (curPMTNum == 7)
              aRCounter->bumpnumPhEnterPMT7QW();
            else if (curPMTNum == 8)
              aRCounter->bumpnumPhEnterPMT8QW();
            else if (curPMTNum == 9)
              aRCounter->bumpnumPhEnterPMT9QW();
            else if (curPMTNum == 10)
              aRCounter->bumpnumPhEnterPMT10QW();
            else if (curPMTNum == 11)
              aRCounter->bumpnumPhEnterPMT11QW();
            else if (curPMTNum == 12)
              aRCounter->bumpnumPhEnterPMT12QW();
            else if (curPMTNum == 13)
              aRCounter->bumpnumPhEnterPMT13QW();
            else if (curPMTNum == 14)
              aRCounter->bumpnumPhEnterPMT14QW();
            else if (curPMTNum == 15)
              aRCounter->bumpnumPhEnterPMT15QW();
          }
        }
      }
    }
  }
  //             G4cout<<"StepAction: End of Step action "<<G4endl;
}

void RichTbSteppingAction::RichTbDebugHisto(const G4Step * /* aStep */) {}

void RichTbSteppingAction::RichTbGenericHisto(const G4Step * /*  aStep */) {}
void RichTbSteppingAction::RichTbRadiatorLensBoundaryIncidenceStep(const G4Step *aStep) {

  RichTbRunConfig *aConfig = RichTbRunConfig::getRunConfigInstance();
  int aRadiatorConfiguration = aConfig->getRadiatorConfiguration();
  if (/*false*/ aRadiatorConfiguration == 3) {
    RichTbRadiatorXNegExtreme_ = RichTbRadiatorXNegExtreme15;
    RichTbRadiatorXPosExtreme_ = RichTbRadiatorXPosExtreme15;
    RichTbRadiatorYNegExtreme_ = RichTbRadiatorYNegExtreme15;
    RichTbRadiatorYPosExtreme_ = RichTbRadiatorYPosExtreme15;
    RichTbRadiatorZNegExtreme_ = RichTbRadiatorZNegExtreme15;
    RichTbRadiatorZPosExtreme_ = RichTbRadiatorZPosExtreme15;
    RichTbRadiatorDnsZLocation_ = RichTbRadiatorDnsZLocation15;
  } else {
    RichTbRadiatorXNegExtreme_ = RichTbRadiatorXNegExtreme;
    RichTbRadiatorXPosExtreme_ = RichTbRadiatorXPosExtreme;
    RichTbRadiatorYNegExtreme_ = RichTbRadiatorYNegExtreme;
    RichTbRadiatorYPosExtreme_ = RichTbRadiatorYPosExtreme;
    RichTbRadiatorZNegExtreme_ = RichTbRadiatorZNegExtreme;
    RichTbRadiatorZPosExtreme_ = RichTbRadiatorZPosExtreme;
    RichTbRadiatorDnsZLocation_ = RichTbRadiatorDnsZLocation;
  }

  G4StepPoint *pPreStepPoint = aStep->GetPreStepPoint();
  G4StepPoint *pPostStepPoint = aStep->GetPostStepPoint();
  const G4ThreeVector prePos = pPreStepPoint->GetPosition();
  const G4ThreeVector postPos = pPostStepPoint->GetPosition();
  RichTbAnalysisManager *myanalysisManager = RichTbAnalysisManager::getInstance();

  // G4cout<<" Now in radiator boundary step "<< prePos <<"   "<<postPos<<  G4endl;
  if ((prePos.z() < RichTbVesselDnsZEnd) &&
      (prePos.x() < RichTbVesselXPosExtreme && prePos.x() > RichTbVesselXNegExtreme) &&
      (prePos.y() < RichTbVesselYPosExtreme && prePos.y() > RichTbVesselYNegExtreme)) {

    if (pPostStepPoint->GetStepStatus() == fGeomBoundary) {

      G4Track *aTrack = aStep->GetTrack();

      const G4DynamicParticle *aParticle = aTrack->GetDynamicParticle();
      const G4double aTrackEnergy = aParticle->GetKineticEnergy();

      //     G4cout<<" Particle definition direction"<< aParticle->GetDefinition()->GetParticleName()
      //      << aTrack->GetMomentum()   <<G4endl;

      if ((aParticle->GetDefinition() == G4OpticalPhoton::OpticalPhoton()) && (aTrackEnergy > 0.0)) {

        if ((prePos.z() < RichTbRadiatorZPosExtreme_) && (prePos.z() > RichTbRadiatorZNegExtreme_) &&
            (prePos.x() < RichTbRadiatorXPosExtreme_) && (prePos.x() > RichTbRadiatorXNegExtreme_) &&
            (prePos.y() < RichTbRadiatorYPosExtreme_) && (prePos.y() > RichTbRadiatorYNegExtreme_)) {
          if (pPreStepPoint->GetPhysicalVolume() && pPostStepPoint->GetPhysicalVolume()) {
            G4String tpreVol = pPreStepPoint->GetPhysicalVolume()->GetName();
            G4String tpostVol = pPostStepPoint->GetPhysicalVolume()->GetName();
            G4ThreeVector pPostStepMom = pPostStepPoint->GetMomentum();
            G4ThreeVector pPreStepMom = pPreStepPoint->GetMomentum();
            G4int PostStepMomZSign = ((pPostStepMom.z()) > 0.0) ? 1 : -1;
            G4int PreStepMomZSign = ((pPreStepMom.z()) > 0.0) ? 1 : -1;
            // G4cout<<" Step action pre post vol" <<tpreVol<<"  "<<tpostVol<<"  "<<prePos<<"   "
            //     <<postPos<<G4endl;

            if ((tpreVol == RadiatorPhysName) && (tpostVol == CrystalMasterPhysName) &&
                (postPos.z() > (RichTbRadiatorDnsZLocation_ - RichTbRadiatorDnsZLocationTolerence)) &&
                (postPos.z() < (RichTbRadiatorDnsZLocation_ + RichTbRadiatorDnsZLocationTolerence))) {

              if (myanalysisManager->getfXYRadiatorDnsPhoton())
                myanalysisManager->getfXYRadiatorDnsPhoton()->Fill(postPos.x(), postPos.y());

              if ((PreStepMomZSign == 1) && (PostStepMomZSign == -1)) {
                //    G4cout<<" TIR step "<<postPos<<G4endl;

                if (myanalysisManager->getfXYRadiatorTIRDnsPhoton())
                  myanalysisManager->getfXYRadiatorTIRDnsPhoton()->Fill(postPos.x(), postPos.y());

                RichTbRadiatorBoundaryInfoAttach(aTrack, postPos, 0);
              } else if ((PreStepMomZSign == 1) && (PostStepMomZSign == 1)) {
                //                        G4cout<<" Refr step "<<postPos<<G4endl;

                if (myanalysisManager->getfXYRadiatorRFRDnsPhoton())
                  myanalysisManager->getfXYRadiatorRFRDnsPhoton()->Fill(postPos.x(), postPos.y());

                RichTbRadiatorBoundaryInfoAttach(aTrack, postPos, 1);
              }

            } else if ((tpreVol == RadiatorPhysName) && (tpostVol == MirrorPhysName)) {
              if ((PreStepMomZSign == -1) && (PostStepMomZSign == 1)) {
                RichTbRadiatorBoundaryInfoAttach(aTrack, postPos, 2);
                // G4cout<<" Mirror step "<<postPos<<G4endl;
              }
            }
          }
        }
      }
    }
  }
}

void RichTbSteppingAction::RichTbPMTIncidenceStep(const G4Step *aStep) {
  G4StepPoint *pPreStepPoint = aStep->GetPreStepPoint();
  G4StepPoint *pPostStepPoint = aStep->GetPostStepPoint();
  const G4ThreeVector prePos = pPreStepPoint->GetPosition();
  const G4ThreeVector postPos = pPostStepPoint->GetPosition();
  // now test if the prestep is inside the vessel.

  if ((prePos.z() < RichTbVesselDnsZEnd && prePos.z() >= 0.0 * CLHEP::mm) &&
      (prePos.x() < RichTbVesselXPosExtreme && prePos.x() > RichTbVesselXNegExtreme) &&
      (prePos.y() < RichTbVesselYPosExtreme && prePos.y() > RichTbVesselYNegExtreme)) {

    // check to see if if we are a boundary.

    if (pPostStepPoint->GetStepStatus() == fGeomBoundary) {

      G4Track *aTrack = aStep->GetTrack();

      const G4DynamicParticle *aParticle = aTrack->GetDynamicParticle();
      const G4double aTrackEnergy = aParticle->GetKineticEnergy();

      // test for photons which are not already killed.

      if ((aParticle->GetDefinition() == G4OpticalPhoton::OpticalPhoton()) && (aTrackEnergy > 0.0)) {

        if (pPreStepPoint->GetPhysicalVolume() && pPostStepPoint->GetPhysicalVolume()) {
          G4String tpreVol = pPreStepPoint->GetPhysicalVolume()->GetName();
          G4String tpostVol = pPostStepPoint->GetPhysicalVolume()->GetName();
          // now for the entry at the wuartz window of hpd.

          if (tpreVol == PMTSMasterPhysName && tpostVol == PMTQuartzPhysName) {
            //   G4cout<<" PMT Incidence Pre post pos xyz "
            //      << prePos.x()<<"  "<<prePos.y()<<"  "<<prePos.z()<<"  "
            //      <<postPos.x() <<"  "<<postPos.y()<<"  "
            //      << postPos.z()<<G4endl;

            RichTbPMTQWIncidenceInfoAttach(aTrack, postPos);
          }
        }
      }
    }
  }
}
