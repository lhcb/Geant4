// Include files

#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "RichTbGeometryParameters.hh"
#include "RichTbMaterial.hh"
#include "RichTbMiscNames.hh"
#include "RichTbRunConfig.hh"

// local
#include "RichTbUpgradePhDetSupFrame.hh"

//-----------------------------------------------------------------------------
// Implementation file for class : RichTbUpgradePhDetSupFrame
//
// 2014-10-23 : Sajan Easo
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichTbUpgradePhDetSupFrame::RichTbUpgradePhDetSupFrame(RichTbUpgradeCrystalMaster *rTbCrysMaster) {
  aRTbCrystalMaster = rTbCrysMaster;

  // constructRichTbPhotoDetectorSupFrame();
}
//=============================================================================
// Destructor
//=============================================================================
RichTbUpgradePhDetSupFrame::~RichTbUpgradePhDetSupFrame() {}

//=============================================================================
void RichTbUpgradePhDetSupFrame::constructRichTbPhotoDetectorSupFrame() {
  RichTbMaterial *aMaterial = RichTbMaterial::getRichTbMaterialInstance();
  //  RichTbRunConfig* aConfig = RichTbRunConfig::  getRunConfigInstance();

  G4Box *PhDetSupFrameBox =
      new G4Box("PhDetSupFrameBox", 0.5 * PhDetSupFrameXSize, 0.5 * PhDetSupFrameYSize, 0.5 * PhDetSupFrameZSize);

  G4RotationMatrix PhDetSupRotX, PhDetSupRotY;

  G4ThreeVector PhDetSupFramePosLeft(PhDetSupFrameXLocation[0], PhDetSupFrameYLocation[0], PhDetSupFrameZLocation);
  G4ThreeVector PhDetSupFramePosRight(PhDetSupFrameXLocation[1], PhDetSupFrameYLocation[1], PhDetSupFrameZLocation);

  G4Transform3D PhDetSupFrameTransformLeft(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosLeft);
  G4Transform3D PhDetSupFrameTransformRight(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosRight);

  G4LogicalVolume *PhDetSupFrameLogLeft =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameLeftLog", 0, 0, 0);

  G4LogicalVolume *PhDetSupFrameLogRight =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameRightLog", 0, 0, 0);

  G4VPhysicalVolume *PhDetSupFramePhysLeft =
      new G4PVPlacement(PhDetSupFrameTransformLeft, PhDetSupPhysNameLeft, PhDetSupFrameLogLeft,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 0);

  G4VPhysicalVolume *PhDetSupFramePhysRight =
      new G4PVPlacement(PhDetSupFrameTransformRight, PhDetSupPhysNameRight, PhDetSupFrameLogRight,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 1);

  RichTbPhDetSupFrameLeftLVol = PhDetSupFrameLogLeft;
  RichTbPhDetSupFrameRightLVol = PhDetSupFrameLogRight;
  RichTbPhDetSupFrameLeftPVol = PhDetSupFramePhysLeft;
  RichTbPhDetSupFrameRightPVol = PhDetSupFramePhysRight;
}

void RichTbUpgradePhDetSupFrame::constructRichTbPhotoDetectorSupFrameWithHpd() {
  RichTbMaterial *aMaterial = RichTbMaterial::getRichTbMaterialInstance();
  //  RichTbRunConfig* aConfig = RichTbRunConfig::  getRunConfigInstance();

  G4Box *PhDetSupFrameBox =
      new G4Box("PhDetSupFrameBox", 0.5 * PhDetSupFrameXSize, 0.5 * PhDetSupFrameYSize, 0.5 * PhDetSupFrameZSize);

  G4RotationMatrix PhDetSupRotX, PhDetSupRotY;

  G4ThreeVector PhDetSupFramePosLeft(PhDetSupFrameXLocation[0], PhDetSupFrameYLocation[0], PhDetSupFrameZLocation);
  G4Transform3D PhDetSupFrameTransformLeft(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosLeft);
  G4LogicalVolume *PhDetSupFrameLogLeft =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameLeftLog", 0, 0, 0);
  G4VPhysicalVolume *PhDetSupFramePhysLeft =
      new G4PVPlacement(PhDetSupFrameTransformLeft, PhDetSupPhysNameLeft, PhDetSupFrameLogLeft,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 0);

  G4Box *PhDetHpdSupFrameBox = new G4Box("PhDetHpdSupFrameBox", 0.5 * HpdPhDetSupFrameXSize,
                                         0.5 * HpdPhDetSupFrameYSize, 0.5 * HpdPhDetSupFrameZSize);

  G4ThreeVector PhDetSupFramePosRight(HpdPhotonDetectorSupFrameXLocation, HpdPhotonDetectorSupFrameYLocation,
                                      HpdPhDetSupFrameZLocation);

  G4RotationMatrix PhDetHpdSupRotX, PhDetHpdSupRotY;

  G4Transform3D PhDetSupFrameTransformRight(PhDetHpdSupRotX * PhDetHpdSupRotY, PhDetSupFramePosRight);

  G4LogicalVolume *PhDetSupFrameLogRight =
      new G4LogicalVolume(PhDetHpdSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameRightLog", 0, 0, 0);

  G4VPhysicalVolume *PhDetSupFramePhysRight =
      new G4PVPlacement(PhDetSupFrameTransformRight, PhDetSupPhysNameRight, PhDetSupFrameLogRight,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 1);

  RichTbPhDetSupFrameLeftLVol = PhDetSupFrameLogLeft;
  RichTbPhDetSupFrameRightLVol = PhDetSupFrameLogRight;
  RichTbPhDetSupFrameLeftPVol = PhDetSupFramePhysLeft;
  RichTbPhDetSupFrameRightPVol = PhDetSupFramePhysRight;
}

void RichTbUpgradePhDetSupFrame::constructRichTbPhotoDetectorSupFrame15() {
  RichTbMaterial *aMaterial = RichTbMaterial::getRichTbMaterialInstance();
  //  RichTbRunConfig* aConfig = RichTbRunConfig::  getRunConfigInstance();

  G4Box *PhDetSupFrameBox =
      new G4Box("PhDetSupFrameBox", 0.5 * PhDetSupFrameXSize, 0.5 * PhDetSupFrameYSize, 0.5 * PhDetSupFrameZSize);

  G4RotationMatrix PhDetSupRotX, PhDetSupRotY;

  G4ThreeVector PhDetSupFramePosLeft(PhDetSupFrameXLocation15[0], PhDetSupFrameYLocation15[0],
                                     PhDetSupFrameZLocation15);
  G4ThreeVector PhDetSupFramePosRight(PhDetSupFrameXLocation15[1], PhDetSupFrameYLocation15[1],
                                      PhDetSupFrameZLocation15);
  G4ThreeVector PhDetSupFramePosBottomLeft(PhDetSupFrameBottomXLocation[0], PhDetSupFrameBottomYLocation[0],
                                           PhDetSupFrameZLocation15);
  G4ThreeVector PhDetSupFramePosBottomRight(PhDetSupFrameBottomXLocation[1], PhDetSupFrameBottomYLocation[1],
                                            PhDetSupFrameZLocation15);

  G4Transform3D PhDetSupFrameTransformLeft(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosLeft);
  G4Transform3D PhDetSupFrameTransformRight(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosRight);
  G4Transform3D PhDetSupFrameTransformBottomLeft(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosBottomLeft);
  G4Transform3D PhDetSupFrameTransformBottomRight(PhDetSupRotX * PhDetSupRotY, PhDetSupFramePosBottomRight);

  G4LogicalVolume *PhDetSupFrameLogLeft =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameLeftLog", 0, 0, 0);
  G4LogicalVolume *PhDetSupFrameLogRight =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameRightLog", 0, 0, 0);
  G4LogicalVolume *PhDetSupFrameLogBottomLeft =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameBottomLeftLog", 0, 0, 0);
  G4LogicalVolume *PhDetSupFrameLogBottomRight =
      new G4LogicalVolume(PhDetSupFrameBox, aMaterial->getNitrogenGas(), "PhDetSupFrameBottomRightLog", 0, 0, 0);

  G4VPhysicalVolume *PhDetSupFramePhysLeft =
      new G4PVPlacement(PhDetSupFrameTransformLeft, PhDetSupPhysNameLeft, PhDetSupFrameLogLeft,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 0);
  G4VPhysicalVolume *PhDetSupFramePhysRight =
      new G4PVPlacement(PhDetSupFrameTransformRight, PhDetSupPhysNameRight, PhDetSupFrameLogRight,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 1);
  G4VPhysicalVolume *PhDetSupFramePhysBottomLeft =
      new G4PVPlacement(PhDetSupFrameTransformBottomLeft, PhDetSupPhysNameBottomLeft, PhDetSupFrameLogBottomLeft,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 0);
  G4VPhysicalVolume *PhDetSupFramePhysBottomRight =
      new G4PVPlacement(PhDetSupFrameTransformBottomRight, PhDetSupPhysNameBottomRight, PhDetSupFrameLogBottomRight,
                        aRTbCrystalMaster->getRichTbUpgradeCrystalMasterPVol(), false, 1);

  RichTbPhDetSupFrameLeftLVol = PhDetSupFrameLogLeft;
  RichTbPhDetSupFrameRightLVol = PhDetSupFrameLogRight;
  RichTbPhDetSupFrameBottomLeftLVol = PhDetSupFrameLogBottomLeft;
  RichTbPhDetSupFrameBottomRightLVol = PhDetSupFrameLogBottomRight;
  RichTbPhDetSupFrameLeftPVol = PhDetSupFramePhysLeft;
  RichTbPhDetSupFrameRightPVol = PhDetSupFramePhysRight;
  RichTbPhDetSupFrameBottomLeftPVol = PhDetSupFramePhysBottomLeft;
  RichTbPhDetSupFrameBottomRightPVol = PhDetSupFramePhysBottomRight;
}
