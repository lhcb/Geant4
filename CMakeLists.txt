#-----------------------------------------------------------------------
# - Top Level CMakeLists.txt for Geant4 Build
#-----------------------------------------------------------------------
# - Enforce an out-of-source builds before anything else
#
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(STATUS "Geant4 requires an out-of-source build.")
  message(STATUS "Please remove these files from ${CMAKE_BINARY_DIR} first:")
  message(STATUS "CMakeCache.txt")
  message(STATUS "CMakeFiles")
  message(STATUS "Once these files are removed, create a separate directory")
  message(STATUS "and run CMake from there")
  message(FATAL_ERROR "in-source build detected")
endif()

#-----------------------------------------------------------------------
# - Define CMake requirements and override make rules as needed
#
cmake_minimum_required(VERSION 3.8...3.18 FATAL_ERROR)
if(${CMAKE_VERSION} VERSION_LESS 3.12)
  cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

# - Make overrides for default flags, so they appear in interfaces
# set(CMAKE_USER_MAKE_RULES_OVERRIDE_CXX
#    ${CMAKE_SOURCE_DIR}/cmake/Modules/G4MakeRules_cxx.cmake)

#-----------------------------------------------------------------------
# - Project definition and basic configuration
#   Version handled manually as project(... VERSION ...) is not used
#   in tests/examples which are subprojects. All calls must use
#   the same form.
project(Geant4)
set(${PROJECT_NAME}_VERSION_MAJOR 10)
set(${PROJECT_NAME}_VERSION_MINOR  7)
set(${PROJECT_NAME}_VERSION_PATCH  3)
set(${PROJECT_NAME}_VERSION "${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH}")

# - Prepend our own CMake Modules to the search path
# NB: if our custom modules include others that we don't supply, those in
# the base path will be used, so watch for incompatibilities!!
#
set(CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/cmake/Modules
    ${CMAKE_MODULE_PATH})

# FIXME: code copied from cmake/Modules/Geant4MakeRules_cxx.cmake waiting for proper refactoring
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang|Intel")
  # available models, default first
  set(_TLSMODELS initial-exec local-exec global-dynamic local-dynamic)
  foreach(_s ${_TLSMODELS})
    set(${_s}_TLSMODEL_FLAGS "-ftls-model=${_s}")
  endforeach()

  list(APPEND _TLSMODELS auto)
  set(auto_TLSMODEL_FLAGS "")

  set(TLSMODEL_IS_AVAILABLE ${_TLSMODELS})

  # FIXME: this is not the right way of enabling threads
  # (see https://cmake.org/cmake/help/latest/module/FindThreads.html)
  set(GEANT4_MULTITHREADED_CXX_FLAGS "-pthread")
endif()


#-----------------------------------------------------------------------
# - Include CMake category main module
#   Factored into category for convenience in tagging
#
include(G4CMakeMain)


# Special settings to support legacy LHCb CMake projects
if(GAUDI_LEGACY_CMAKE_SUPPORT)
  set(PROJECT_VERSION ${${PROJECT_NAME}_VERSION})
  include(${legacy_cmake_config_support})
  # This is to match Geant4 install layout
  install(FILES
    ${PROJECT_BINARY_DIR}/.metadata.cmake
    ${PROJECT_SOURCE_DIR}/source/lhcb/Geant4.xenv
    DESTINATION ${GEANT4_CMAKE_DIR}
    COMPONENT Development
  )
endif()
