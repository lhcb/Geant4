#ifndef INCLUDE_RICHTBSTEPPINGACTIONBASE_HH
#define INCLUDE_RICHTBSTEPPINGACTIONBASE_HH 1

// Include files
#include "G4UserSteppingAction.hh"
#include "G4VDiscreteProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "G4Step.hh"

#include "RichTbSteppingAction.hh"
#include "RichTbPmtSteppingAction.hh"

/** @class RichTbSteppingActionBase RichTbSteppingActionBase.hh include/RichTbSteppingActionBase.hh
 *
 *
 *  @author Sajan Easo
 *  @date   2015-03-06
 */
class RichTbSteppingActionBase : public G4UserSteppingAction {
public:
  /// Standard constructor
  RichTbSteppingActionBase();

  virtual ~RichTbSteppingActionBase(); ///< Destructor
  void UserSteppingAction(const G4Step *aStep) override;
  void InitRichTbStepActions();

protected:
private:
  RichTbSteppingAction *mRichTbSteppingAction;
  RichTbPmtSteppingAction *mRichTbPmtSteppingAction;
};
#endif // INCLUDE_RICHTBSTEPPINGACTIONBASE_HH
