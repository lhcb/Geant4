2024-02-02 Geant4 v10r7p3t1
===

This is the build of Geant4 in the LHCb environment with LHCb patches and tests.

This version is based on  10.7.p03
([original release notes](ReleaseNotes4.10.7.html) and [patch notes](Patch4.10.7-3.txt))

This version uses
LCG [104](http://lcginfo.cern.ch/release/104/) with ROOT 6.28.04.

This version is released on `master` branch.
Built relative to Geant4 [v10r7p3t0.md](/../../tags/v10r7p3t0.md).
