// $Id: $
// Include files

// local
#include "RichTbBeamProperty.hh"
#include "G4OpticalPhoton.hh"
#include "G4ParticleDefinition.hh"
#include "G4PionMinus.hh"
#include "G4PionPlus.hh"
#include "G4Proton.hh"
#include "RichTbGeometryParameters.hh"

//-----------------------------------------------------------------------------
// Implementation file for class : RichTbBeamProperty
//
// 2004-01-22 : Sajan EASO
//-----------------------------------------------------------------------------
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichTbBeamProperty *RichTbBeamProperty::RichTbBeamPropertyInstance = 0;

RichTbBeamProperty::RichTbBeamProperty() {

  ResetBeamProperty();
  mNominalBeamPosition = G4ThreeVector(RichTbNominalBeamXPos, RichTbNominalBeamYPos, RichTbNominalBeamZPos);

  mNominalBeamDirectionCos =
      G4ThreeVector(RichTbNominalBeamDirCosX, RichTbNominalBeamDirCosY, RichTbNominalBeamDirCosZ);
}
//=============================================================================
// Destructor
//=============================================================================
RichTbBeamProperty::~RichTbBeamProperty() {}

//=============================================================================
RichTbBeamProperty *RichTbBeamProperty::getRichTbBeamPropertyInstance() {
  if (RichTbBeamPropertyInstance == 0) {
    RichTbBeamPropertyInstance = new RichTbBeamProperty();
  }
  return RichTbBeamPropertyInstance;
}
void RichTbBeamProperty::ResetBeamProperty() {
  mBeamPosition = G4ThreeVector(0.0, 0.0, 0.0);
  mBeamDirection = G4ThreeVector(0.0, 0.0, 1.0);
  mBeamPartDef = G4PionMinus::PionMinusDefinition();
  mBeamPartName = "pi-";
  mBeamPosUpstrAgel = G4ThreeVector(0.0, 0.0, 0.0);
  mBeamDirUpstrAgel = G4ThreeVector(0.0, 0.0, 1.0);
  mAgelNormal = G4ThreeVector(0.0, 0.0, 1.0);
}
void RichTbBeamProperty::PrintBeamProperty() {
  G4cout << "BeamPos XYZ    " << mBeamPosition.x() << "    " << mBeamPosition.y() << "    " << mBeamPosition.z()
         << G4endl;
  G4cout << "BeamDirection XYZ  AgelNormal XYZ  " << mBeamDirection.x() << "    " << mBeamDirection.y() << "    "
         << mBeamDirection.z() << "   " << mAgelNormal.x() << "   " << mAgelNormal.y() << "    " << mAgelNormal.z()
         << G4endl;
  G4cout << "Beam Particle Name is  " << mBeamPartName << G4endl;
}
